stages:
  - environment
  - build
  - test
  - review
  - security
  - internal
  - alpha
  - beta
  - production
  - stop

.updateContainerJob:
  image: docker:stable
  stage: environment
  services:
    - docker:dind
  script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker pull $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG || true
    - docker build --cache-from $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG -t $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG .
    - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG


.use-docker-in-docker:
  image: docker:${DOCKER_VERSION}
  services:
    - docker:${DOCKER_VERSION}-dind
  variables:
    DOCKER_DRIVER: overlay2
    DOCKER_HOST: tcp://docker:2375
    DOCKER_TLS_CERTDIR: ''
  tags:
    # See https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7019 for tag descriptions
    - gitlab-org-docker

updateContainer:
  extends: .updateContainerJob
  only:
    changes:
      - Dockerfile

ensureContainer:
  extends: .updateContainerJob
  allow_failure: true
  before_script:
    - "mkdir -p ~/.docker && echo '{\"experimental\": \"enabled\"}' > ~/.docker/config.json"
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    # Skip update container `script` if the container already exists
    # via https://gitlab.com/gitlab-org/gitlab-ce/issues/26866#note_97609397 -> https://stackoverflow.com/a/52077071/796832
    - docker manifest inspect $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG > /dev/null && exit || true


.build_job:
  image: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  stage: build
  before_script:
    - wget --output-document=artifacts.zip "https://gitlab.com/gitlab-org/gitter/webapp/-/jobs/artifacts/master/download?job=mobile-asset-build"
    - unzip artifacts.zip
    - mkdir -p app/src/main/assets/www
    - mv output/android/www/* app/src/main/assets/www/
    # We store this binary file in a variable as hex with this command, `xxd -p gitter-android-app.jks`
    # Then we convert the hex back to a binary file
    - echo "$signing_jks_file_hex" | xxd -r -p - > android-signing-keystore.jks
    # We add 100 to get this high enough above current versionCodes that are published
    - "export VERSION_CODE=$((100 + $CI_PIPELINE_IID)) && echo $VERSION_CODE"
    - "export VERSION_SHA=`echo ${CI_COMMIT_SHA:0:8}` && echo $VERSION_SHA"
    # Move current version to a new release notes for fastlane
    # This happens for every build, but only on master and tags this change gets submitted as an MR by `buildCreateReleaseNotes` task
    - mv ./fastlane/metadata/android/en-GB/changelogs/CURRENT_VERSION.txt "./fastlane/metadata/android/en-GB/changelogs/$VERSION_CODE.txt"
  after_script:
    - rm -f android-signing-keystore.jks || true
  artifacts:
    paths:
    - app/build/outputs

buildDebug:
  extends: .build_job
  script:
    - bundle exec fastlane buildDebug

buildCreateReleaseNotes:
  extends: .build_job
  script:
    - ./ci-scripts/create-changelog-mr.sh
  only:
    - tags

buildRelease:
  extends: .build_job
  script:
    - bundle exec fastlane buildRelease
  environment:
    name: production

deployReview:
  stage: review
  image: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  environment:
    name: review/$CI_COMMIT_REF_NAME
    url: https://appetize.io/app/$APPETIZE_PUBLIC_KEY
    on_stop: stopReview
  when: manual
  script:
    - bundle exec fastlane review
  only:
    - branches
  except:
    - master
  artifacts:
    paths:
      - appetize-information.json
    reports:
      dotenv: deploy.env

stopReview:
  stage: stop
  environment:
    name: review/$CI_COMMIT_REF_NAME
    action: stop
  variables:
    GIT_STRATEGY: none
  when: manual
  only:
    - branches
  except:
    - master
  script:
    - apt-get -y update && apt-get -y upgrade && apt-get -y install jq curl
    - curl --request DELETE https://$APPETIZE_TOKEN@api.appetize.io/v1/apps/`jq -r '.publicKey' < appetize-information.json`

testDebug:
  image: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  stage: test
  dependencies:
    - buildDebug
  script:
    - bundle exec fastlane test

publishInternal:
  image: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  stage: internal
  dependencies:
    - buildRelease
  when: manual
  before_script:
    - echo $google_play_service_account_api_key_json > ~/google_play_api_key.json
  after_script:
    - rm -f ~/google_play_api_key.json
  script:
    - bundle exec fastlane internal

.promote_job:
  image: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  when: manual
  dependencies: []
  before_script:
    - echo $google_play_service_account_api_key_json > ~/google_play_api_key.json
  after_script:
    - rm -f ~/google_play_api_key.json

promoteAlpha:
  extends: .promote_job
  stage: alpha
  script:
    - bundle exec fastlane promote_internal_to_alpha

promoteBeta:
  extends: .promote_job
  stage: beta
  script:
    - bundle exec fastlane promote_alpha_to_beta

promoteProduction:
  extends: .promote_job
  stage: production
  # We only allow production promotion on `master` because
  # it has its own production scoped secret variables
  only:
   - tags
  script:
    - bundle exec fastlane promote_beta_to_production



#
# Security reports
# =========================================================
include:
  - template: Security/SAST.gitlab-ci.yml
  - template: Security/Dependency-Scanning.gitlab-ci.yml
  - template: Secret-Detection.gitlab-ci.yml


dependency_scanning:
  extends: .use-docker-in-docker
  stage: security
  rules:
    - when: always
  variables:
    DS_DEFAULT_ANALYZERS: "bundler-audit,gemnasium-maven"
    DS_DISABLE_DIND: "true"

sast:
  extends: .use-docker-in-docker
  stage: security
  rules:
    - when: always
  variables:
    SAST_EXCLUDED_PATHS: '/test/**,**/*-test.js,/scripts,/build-scripts'
    FAIL_NEVER: 1

